package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Offer implements Parcelable {

    private Integer id;

    private Drug drug;

    private Store store;

    private String status;

    private String price;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.drug, flags);
        dest.writeParcelable(this.store, flags);
        dest.writeString(this.status);
        dest.writeString(this.price);
    }

    public Offer() {
    }

    protected Offer(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.drug = in.readParcelable(Drug.class.getClassLoader());
        this.store = in.readParcelable(Store.class.getClassLoader());
        this.status = in.readString();
        this.price = in.readString();
    }

    public static final Creator<Offer> CREATOR = new Creator<Offer>() {
        @Override
        public Offer createFromParcel(Parcel source) {
            return new Offer(source);
        }

        @Override
        public Offer[] newArray(int size) {
            return new Offer[size];
        }
    };
}
