package com.example.serdar.userside.Utils;

public class Constants {

    public static final String DURATION_STATUS = "duration_status";
    public static final String DURATION_DATE="duration_date";
    public static final String DURATION_DAY="duration_day";

    public static final String NO_END_DATE="no_end_date";
    public static final String UNTIL_DATE="until_date";
    public static final String FOR_X_DAY="for_x_day";

    public static final String BASE_URL="https://drug-store-project.herokuapp.com";


    public static final String USER_ID ="user_id";
    public static final String USER_TOKEN ="user_token";

    public static final String LATITUDE ="latitude";
    public static final String LONGITUDE ="longitude";

    public static final String OFFER_LIST="offer_list";

    public static final String ADDRESS_TITLE="address_title";



}
