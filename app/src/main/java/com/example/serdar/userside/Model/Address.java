package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Address implements Parcelable {

    private Integer id;

    private Integer store;

    private String line;

    private String latitude;

    private String longitude;

    private String phone;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStore() {
        return store;
    }

    public void setStore(Integer store) {
        this.store = store;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Address() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.store);
        dest.writeString(this.line);
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.phone);
    }

    protected Address(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.store = (Integer) in.readValue(Integer.class.getClassLoader());
        this.line = in.readString();
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.phone = in.readString();
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel source) {
            return new Address(source);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };
}
