package com.example.serdar.userside.Storage;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.serdar.userside.Model.Therapy;

import java.util.List;

@Dao
public interface TherapyDoa {

    @Insert
    void insert(Therapy therapy);

    @Update
    void update(Therapy therapy);

    @Delete
    void delete(Therapy therapy);

    @Query("DELETE FROM therapy_table")
    void deleteAllTherapies();

    @Query("SELECT * FROM therapy_table ORDER BY id DESC")
    LiveData<List<Therapy>> getAllTherapies();

    @Query("DELETE FROM therapy_table WHERE id =:id")
    void deleteTherapyById(Integer id);


    @Query("UPDATE therapy_table SET rtMorning=:morningValue WHERE id= :id")
    void updatePillStatus(String morningValue, Integer id);


}
