package com.example.serdar.userside.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.serdar.userside.Model.SharedPrefLocation;
import com.example.serdar.userside.Model.SharedPrefUser;

public class SharedPrefManager {


    private static final String SHARED_PREF_NAME = "my_shared_pref";
    private static  SharedPrefManager Instance;
    private Context mContext;
    private  static SharedPreferences sharedPreferences;


    private SharedPrefManager(Context mCtx) {
        this.mContext = mCtx;
    }

    public static synchronized SharedPrefManager getInstance(Context mCtx) {

        if (Instance == null) {
            sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            Instance = new SharedPrefManager(mCtx);
        }
        return Instance;
    }

    public void saveUser(int user_id, String user_token ) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Constants.USER_ID, user_id);
        editor.putString(Constants.USER_TOKEN, user_token);
        editor.apply();
    }

    public void saveLangLong(String latitude, String longitude){
        SharedPreferences.Editor editor=sharedPreferences.edit();

        editor.putString(Constants.LATITUDE,latitude);
        editor.putString(Constants.LONGITUDE,longitude);
        editor.apply();
    }

    public SharedPrefLocation getLocation(){
        return new SharedPrefLocation(
                sharedPreferences.getString(Constants.LATITUDE,null),
                sharedPreferences.getString(Constants.LONGITUDE,null)
        );
    }

    public SharedPrefUser getUser() {
        return new SharedPrefUser(
                sharedPreferences.getInt(Constants.USER_ID, -1),
                sharedPreferences.getString(Constants.USER_TOKEN, null)
        );
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getInt(Constants.USER_ID, -1) != -1;
    }

    public void clear(){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
