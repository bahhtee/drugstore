package com.example.serdar.userside.Storage;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.serdar.userside.Model.Therapy;

@Database(entities = {Therapy.class},version = 2)
public  abstract class MyDrugStoreDatabase extends RoomDatabase {

    private static MyDrugStoreDatabase instance;

    public abstract TherapyDoa mTherapyDoa();

    public static MyDrugStoreDatabase getDatabase(final Context context){
        if(instance==null){
            synchronized (MyDrugStoreDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            MyDrugStoreDatabase.class, "my_drug_store_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(roomCallback)
                            .build();
                }
            }
        }
        return instance;
    }

    private static RoomDatabase.Callback roomCallback=new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);
        }
    };
}
