package com.example.serdar.userside.Fragments;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.serdar.userside.Model.Therapy;
import com.example.serdar.userside.R;
import com.example.serdar.userside.Storage.TherapyViewModel;
import com.example.serdar.userside.TherapyAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewTherapyFragment extends Fragment implements TherapyAdapter.OnItemClickListener {

    private static final String TAG = "ViewTherapyFragment";

    private RecyclerView mRecyclerView;
    private TherapyAdapter mTherapyAdapter;
    private List<Therapy> mTherapies=new ArrayList<Therapy>();
    private TherapyViewModel mTherapyViewModel;
    private RelativeLayout mRelativeLayout;
    private FloatingActionButton fab;



    public interface OnSelectPillListener{
        public void onSelectPill();
    }

    public interface OnTherapySelectedListener{
        public void oOTherapySelected(Therapy therapy);
    }

    OnSelectPillListener mOnSelectPillListener;
    OnTherapySelectedListener mOnTherapySelected;

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.view_therapy_fragment,container,false);
        mRecyclerView=(RecyclerView)view.findViewById(R.id.therapyRecycler);
        mRelativeLayout=(RelativeLayout)view.findViewById(R.id.relLayout2);
        fab = (FloatingActionButton)view.findViewById(R.id.fab);
        mTherapyViewModel= ViewModelProviders.of(this).get(TherapyViewModel.class);
        mTherapyViewModel.getAllTherapies().observe(this, new Observer<List<Therapy>>() {
            @Override
            public void onChanged(@Nullable List<Therapy> therapies) {
                if(therapies.size()>0){
                    mTherapies.addAll(therapies);
                    fab.setVisibility(View.VISIBLE);
                    mRecyclerView.setHasFixedSize(true);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mTherapyAdapter=new TherapyAdapter(getActivity(),therapies);
                    mRecyclerView.setAdapter(mTherapyAdapter);
                    mTherapyAdapter.setOnItemClickListener(ViewTherapyFragment.this);
                }else{
                    if(mRelativeLayout.getVisibility()==View.GONE){
                       mRelativeLayout.setVisibility(View.VISIBLE);
                        fab.setVisibility(View.GONE);
                    }
                }
            }
        });


        //navigation for the backarrow
        ImageButton imgBackArrow = (ImageButton)view.findViewById(R.id.ibBackArrow);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                getActivity().finish();

            }
        });



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnSelectPillListener.onSelectPill();
            }
        });

        Button btnCreateTherapy=(Button)view.findViewById(R.id.btnCreateTherapy);
        btnCreateTherapy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnSelectPillListener.onSelectPill();
            }
        });


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try{
            mOnSelectPillListener=(OnSelectPillListener)getActivity();
            mOnTherapySelected=(OnTherapySelectedListener)getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException "+e.getMessage() );
        }
    }


    @Override
    public void onItemClick(int position) {
        Therapy current_therapy=mTherapies.get(position);
        mOnTherapySelected.oOTherapySelected(current_therapy);
    }

}
