package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Producer implements Parcelable {

    private Integer id;

    private String title;

    private String country;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeString(this.country);
    }

    public Producer() {
    }

    protected Producer(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.country = in.readString();
    }

    public static final Creator<Producer> CREATOR = new Creator<Producer>() {
        @Override
        public Producer createFromParcel(Parcel source) {
            return new Producer(source);
        }

        @Override
        public Producer[] newArray(int size) {
            return new Producer[size];
        }
    };

    @Override
    public String toString() {
        return title;
    }
}
