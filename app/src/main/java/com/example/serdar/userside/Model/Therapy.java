package com.example.serdar.userside.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "therapy_table")
public class Therapy implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name="id")
    private Integer therapyId;

    @ColumnInfo(name="pillName")
    private String pillName;

    @ColumnInfo(name="pillCompany")
    private String pillCompany;

    @ColumnInfo(name = "pillUnit")
    private String pillUnit;

    @ColumnInfo(name = "intakeAdvice")
    private String intakeAdvice;

    @ColumnInfo(name="startDate")
    private String startDate;

    @ColumnInfo(name="noEndDate")
    private String noEndDate;

    @ColumnInfo(name ="untilDate")
    private String untilDate;

    @ColumnInfo(name="forXDay")
    private int forXDay;


    @ColumnInfo(name="rtMorning")
    private String rtMorning;

    @ColumnInfo(name="rtLunch")
    private String rtLunch;

    @ColumnInfo(name="rtEvening")
    private String rtEvening;

    @ColumnInfo(name="rtNight")
    private String rtNight;

    @ColumnInfo(name="remainderTime")
    private String remainderTime;


    public String getRemainderTime() {
        return remainderTime;
    }


    public void setRemainderTime(String remainderTime) {
        this.remainderTime = remainderTime;
    }

    public Integer getTherapyId() {
        return therapyId;
    }

    public void setTherapyId(Integer therapyId) {
        this.therapyId = therapyId;
    }

    public String getPillName() {
        return pillName;
    }

    public void setPillName(String pillName) {
        this.pillName = pillName;
    }

    public String getPillCompany() {
        return pillCompany;
    }

    public void setPillCompany(String pillCompany) {
        this.pillCompany = pillCompany;
    }

    public String getPillUnit() {
        return pillUnit;
    }

    public void setPillUnit(String pillUnit) {
        this.pillUnit = pillUnit;
    }

    public String getIntakeAdvice() {
        return intakeAdvice;
    }

    public void setIntakeAdvice(String intakeAdvice) {
        this.intakeAdvice = intakeAdvice;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getNoEndDate() {
        return noEndDate;
    }

    public void setNoEndDate(String noEndDate) {
        this.noEndDate = noEndDate;
    }

    public String getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(String untilDate) {
        this.untilDate = untilDate;
    }

    public int getForXDay() {
        return forXDay;
    }

    public void setForXDay(int forXDay) {
        this.forXDay = forXDay;
    }

    public String getRtMorning() {
        return rtMorning;
    }

    public void setRtMorning(String rtMorning) {
        this.rtMorning = rtMorning;
    }

    public String getRtLunch() {
        return rtLunch;
    }

    public void setRtLunch(String rtLunch) {
        this.rtLunch = rtLunch;
    }

    public String getRtEvening() {
        return rtEvening;
    }

    public void setRtEvening(String rtEvening) {
        this.rtEvening = rtEvening;
    }

    public String getRtNight() {
        return rtNight;
    }

    public void setRtNight(String rtNight) {
        this.rtNight = rtNight;
    }


    public Therapy(String pillName, String pillCompany, String intakeAdvice, String pillUnit, int forXDay, String untilDate,String noEndDate, String remainderTime) {
        this.pillName = pillName;
        this.pillCompany = pillCompany;
        this.pillUnit = pillUnit;
        this.intakeAdvice = intakeAdvice;
        this.noEndDate=noEndDate;
        this.untilDate = untilDate;
        this.forXDay = forXDay;
        this.remainderTime = remainderTime;
    }

    public Therapy() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.therapyId);
        dest.writeString(this.pillName);
        dest.writeString(this.pillCompany);
        dest.writeString(this.pillUnit);
        dest.writeString(this.intakeAdvice);
        dest.writeString(this.startDate);
        dest.writeString(this.noEndDate);
        dest.writeString(this.untilDate);
        dest.writeInt(this.forXDay);
        dest.writeString(this.rtMorning);
        dest.writeString(this.rtLunch);
        dest.writeString(this.rtEvening);
        dest.writeString(this.rtNight);
        dest.writeString(this.remainderTime);
    }

    protected Therapy(Parcel in) {
        this.therapyId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pillName = in.readString();
        this.pillCompany = in.readString();
        this.pillUnit = in.readString();
        this.intakeAdvice = in.readString();
        this.startDate = in.readString();
        this.noEndDate = in.readString();
        this.untilDate = in.readString();
        this.forXDay = in.readInt();
        this.rtMorning = in.readString();
        this.rtLunch = in.readString();
        this.rtEvening = in.readString();
        this.rtNight = in.readString();
        this.remainderTime = in.readString();
    }

    public static final Creator<Therapy> CREATOR = new Creator<Therapy>() {
        @Override
        public Therapy createFromParcel(Parcel source) {
            return new Therapy(source);
        }

        @Override
        public Therapy[] newArray(int size) {
            return new Therapy[size];
        }
    };
}
