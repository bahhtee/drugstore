package com.example.serdar.userside.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.serdar.userside.Model.SharedPreferenceDurationModel.SharedPreferenceDuration;

import java.util.Date;

public class SharedPreferenceManager {
    private static final String SHARED_PREF_NAME = "my_shared_pref";
    private static  SharedPreferenceManager Instance;
    private Context mContext;
    private  static SharedPreferences sharedPreferences;


    private SharedPreferenceManager(Context mCtx) {
        this.mContext = mCtx;
    }

    public static synchronized SharedPreferenceManager getInstance(Context mCtx) {

        if (Instance == null) {
            sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            Instance = new SharedPreferenceManager(mCtx);
        }
        return Instance;
    }

    public void saveDuration(String duration_status, String no_end_date,String until_date,int for_x_day){
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(Constants.DURATION_STATUS,duration_status);
        editor.putString(Constants.NO_END_DATE,no_end_date);
        editor.putString(Constants.UNTIL_DATE,until_date);
        editor.putInt(Constants.FOR_X_DAY,for_x_day);
        editor.apply();


    }

    public SharedPreferenceDuration getDuration(){
        return new SharedPreferenceDuration(
                sharedPreferences.getString(Constants.DURATION_STATUS,"undefined"),
                sharedPreferences.getString(Constants.NO_END_DATE,"undefined"),
                sharedPreferences.getString(Constants.UNTIL_DATE,"undefined"),
                sharedPreferences.getInt(Constants.FOR_X_DAY,-1)
        );
    }

}
