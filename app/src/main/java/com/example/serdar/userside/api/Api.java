package com.example.serdar.userside.api;

import com.example.serdar.userside.Model.Offer;
import com.example.serdar.userside.Model.Region;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface Api {

    //these two GET requests is used for requesting region list and offer list from server

    @GET("/api/region/list")
    Call<ArrayList<Region>> getRegionList();

    @GET("/api/offer/list")
    Call<ArrayList<Offer>> getOfferList(@QueryMap Map<String, String> parameters);

}
