package com.example.serdar.userside.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.serdar.userside.Model.Therapy;
import com.example.serdar.userside.R;

public class SelectTherapyFragment extends Fragment  implements AdapterView.OnItemSelectedListener {
    private static final String TAG = "SelectTherapyFragment";
    
    private Spinner mSpinner;
    private EditText mEditTextPillName,mEditTextPillComapny;
    private Button mButtonNext;
    private TextView txtIntakeAdvice;
    private  String pill_unit, intake_advice;
    private Therapy mTherapy;


    public interface OnSelectedIntakeAdviceFragmentListener {
        public void onSelectedIntakeAdviceFragment(Therapy therapy);
    }

    OnSelectedIntakeAdviceFragmentListener mOnSelectedIntakeAdviceFragmentListener;

    public interface OnEnteredTherapyListener{
        public void onEnteredTherapy(Therapy therapy);
    }

    OnEnteredTherapyListener mOnEnteredTherapyListener;

    //this will evade the nullpointer exception when adding to  a new  bundle from MyTherapyActivity
    public SelectTherapyFragment(){
        super();
        setArguments(new Bundle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.add_theraphy_fragment,container,false);
        mTherapy=getTherapyFromBundle();


        mSpinner=(Spinner)view.findViewById(R.id.spinner_unit);

        mEditTextPillName=(EditText)view.findViewById(R.id.pill_name);
        mEditTextPillName.setSelection(mEditTextPillName.getText().length());

        mEditTextPillComapny=(EditText)view.findViewById(R.id.pill_company_name);
        mEditTextPillComapny.setSelection(mEditTextPillComapny.getText().length());

        mButtonNext=(Button)view.findViewById(R.id.btn_next);
        txtIntakeAdvice=(TextView)view.findViewById(R.id.intake_advice_value);

        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(getActivity(),R.array.units,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(this);

        if(mTherapy!=null){
            txtIntakeAdvice.setText(mTherapy.getIntakeAdvice());
            mEditTextPillName.setText(mTherapy.getPillName());
            mEditTextPillComapny.setText(mTherapy.getPillCompany());
            int position=adapter.getPosition(mTherapy.getPillUnit());
            mSpinner.setSelection(position);
        }else{
            txtIntakeAdvice.setText("None");
        }

        intake_advice=txtIntakeAdvice.getText().toString().trim();


        //navigation for the backarrow
        ImageButton imgBackArrow = (ImageButton)view.findViewById(R.id.ibBackArrow);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked back arrow.");
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();

                //hide the keyboard
                View view=getView();
                InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                try {
                    imm.hideSoftInputFromWindow(view.getWindowToken(),0);
                }catch (NullPointerException e){
                    Log.d(TAG, "setAppBarState: NullPointerException"+e.getMessage());
                }

            }
        });

        LinearLayout clickableLayout=(LinearLayout)view.findViewById(R.id.linLayout_intake_advice);
        clickableLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Therapy therapy=new Therapy();
                therapy.setPillName(mEditTextPillName.getText().toString().trim());
                therapy.setPillCompany(mEditTextPillComapny.getText().toString().trim());
                therapy.setPillUnit(pill_unit);
                mOnSelectedIntakeAdviceFragmentListener.onSelectedIntakeAdviceFragment(therapy);
                hideKeyboard();
            }
        });


        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pill_name=mEditTextPillName.getText().toString().trim();
                String pill_company=mEditTextPillComapny.getText().toString().trim();
                if(pill_name.isEmpty()){
                    mEditTextPillName.setError("название лекарства не может быть пустым");
                    mEditTextPillName.requestFocus();
                    return;
                }else if(pill_company.isEmpty()){
                    mEditTextPillComapny.setError("компания производитель не может быть пустым");
                    mEditTextPillComapny.requestFocus();
                    return;
                }
                Therapy therapy=new Therapy();
                therapy.setPillName(pill_name);
                therapy.setPillCompany(pill_company);
                therapy.setPillUnit(pill_unit);
                therapy.setIntakeAdvice(intake_advice);

                mOnEnteredTherapyListener.onEnteredTherapy(therapy);

                hideKeyboard();

            }
        });

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        pill_unit=parent.getItemAtPosition(position).toString();
        Toast.makeText(getActivity(), pill_unit, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnSelectedIntakeAdviceFragmentListener =(OnSelectedIntakeAdviceFragmentListener)getActivity();
            mOnEnteredTherapyListener=(OnEnteredTherapyListener)getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException "+e.getMessage() );
        }
    }

    /**
     * Retrieves the selected advice from the bundle (coming from TherapyActivity)
     * @return
     */
    private Therapy getTherapyFromBundle(){
        Log.d(TAG, "getAdviceFromBundle: arguments: "+getArguments());

        Bundle bundle=this.getArguments();
        if(bundle!=null){
            return  bundle.getParcelable(getString(R.string.therapy));
        }else {
            return null;
        }
    }

    private void hideKeyboard(){
        //hide the keyboard
        View view=getView();
        InputMethodManager imm=(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);
        }catch (NullPointerException e){
            Log.d(TAG, "setAppBarState: NullPointerException"+e.getMessage());
        }
    }
}
