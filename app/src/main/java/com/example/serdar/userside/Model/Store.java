package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Store implements Parcelable {

    private Integer id;

    private String email;

    private String title;

    private String password;

    private int rate;

    private Region region;

    private boolean is_active;

    private boolean staff;

    private ArrayList<Address> address;

    private boolean admin;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public boolean isIs_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean isStaff() {
        return staff;
    }

    public void setStaff(boolean staff) {
        this.staff = staff;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public ArrayList<Address> getAddress() {
        return address;
    }

    public void setAddress(ArrayList<Address> address) {
        this.address = address;
    }

    public Store() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.email);
        dest.writeString(this.title);
        dest.writeString(this.password);
        dest.writeInt(this.rate);
        dest.writeParcelable(this.region, flags);
        dest.writeByte(this.is_active ? (byte) 1 : (byte) 0);
        dest.writeByte(this.staff ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.address);
        dest.writeByte(this.admin ? (byte) 1 : (byte) 0);
    }

    protected Store(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.title = in.readString();
        this.password = in.readString();
        this.rate = in.readInt();
        this.region = in.readParcelable(Region.class.getClassLoader());
        this.is_active = in.readByte() != 0;
        this.staff = in.readByte() != 0;
        this.address = in.createTypedArrayList(Address.CREATOR);
        this.admin = in.readByte() != 0;
    }

    public static final Creator<Store> CREATOR = new Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel source) {
            return new Store(source);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}
