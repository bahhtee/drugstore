package com.example.serdar.userside;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.serdar.userside.Fragments.AddMedicationFragment;
import com.example.serdar.userside.Fragments.IntakeAdviceFragment;
import com.example.serdar.userside.Fragments.SelectTherapyFragment;
import com.example.serdar.userside.Fragments.UpdateMedicationFragment;
import com.example.serdar.userside.Fragments.ViewTherapyFragment;
import com.example.serdar.userside.Model.Therapy;

public class MyTherapy extends AppCompatActivity implements ViewTherapyFragment.OnSelectPillListener,
        SelectTherapyFragment.OnSelectedIntakeAdviceFragmentListener,
        IntakeAdviceFragment.OnSelectedAdviceListener,
        SelectTherapyFragment.OnEnteredTherapyListener,
        ViewTherapyFragment.OnTherapySelectedListener {

    private static final String TAG = "MyTherapy";

    private long backPressedTime;
    private Toast backToast;


    @Override
    public void onSelectPill() {
        Log.d(TAG, "onSelectPill: navigating to " + getString(R.string.select_pill_fragment));

        SelectTherapyFragment fragment = new SelectTherapyFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(getString(R.string.select_pill_fragment));
        transaction.commit();
    }


    @Override
    public void onSelectedIntakeAdviceFragment(Therapy therapy) {
        IntakeAdviceFragment fragment = new IntakeAdviceFragment();
        Bundle args=new Bundle();
        args.putParcelable(getString(R.string.therapy),therapy);
        fragment.setArguments(args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(getString(R.string.intake_advice_fragment));
        transaction.commit();
    }


    @Override
    public void onSelectedAdvice(Therapy therapy) {

        SelectTherapyFragment fragment=new SelectTherapyFragment();
        Bundle args=new Bundle();
        args.putParcelable(getString(R.string.therapy),therapy);
        fragment.setArguments(args);

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(getString(R.string.intake_advice_fragment));
        transaction.commit();


    }


    @Override
    public void onEnteredTherapy(Therapy therapy) {
        AddMedicationFragment fragment=new AddMedicationFragment();
        Bundle args=new Bundle();
        args.putParcelable(getString(R.string.therapy),therapy);
        fragment.setArguments(args);

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(getString(R.string.add_medication_fragment));
        transaction.commit();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_therapy_activity);

        init();
    }

    private void init() {
        ViewTherapyFragment fragment = new ViewTherapyFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            finish();
            return;
        } else {
            backToast = Toast.makeText(getBaseContext(),  "Нажмите назад еще раз чтобы выйти", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    public void oOTherapySelected(Therapy therapy) {

        UpdateMedicationFragment fragment=new UpdateMedicationFragment();
        Bundle args=new Bundle();
        args.putParcelable(getString(R.string.therapy),therapy);
        fragment.setArguments(args);

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack(getString(R.string.add_medication_fragment));
        transaction.commit();

    }
}
