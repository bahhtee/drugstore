package com.example.serdar.userside.Fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.serdar.userside.Model.Therapy;
import com.example.serdar.userside.R;
import com.example.serdar.userside.Storage.TherapyViewModel;
import com.example.serdar.userside.Utils.AlertReceiver;
import com.example.serdar.userside.Utils.Constants;
import com.example.serdar.userside.Utils.SharedPreferenceManager;

import java.util.Calendar;

public class UpdateMedicationFragment extends Fragment {

    private static final String TAG = "AddMedicationFragment";

    private Therapy mTherapy;
    private TextView pillName, pillCompany, pillUnit, pillAdvice, pillDuration, chosenDurationStatus;
    private LinearLayout remainderTime, durationLayout;
    private NumberPicker mNumberPickerHour, mNumberPickerMinute;
    private TherapyViewModel mTherapyViewModel;
    private int hour, minute;
    private Toolbar mToolbar;
    private SharedPreferenceManager pref;
    private  String time_period;

    //this will evade the null pointer exception when adding to  a new  bundle from MyTherapyActivity
    public UpdateMedicationFragment() {
        super();
        setArguments(new Bundle());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.update_medication_fragment, container, false);
        mTherapy = getTherapyFromBundle();

        mToolbar=view.findViewById(R.id.viewUpdateTherapyToolbar);
        // required for setting up the toolbar
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        setHasOptionsMenu(true);
        mTherapyViewModel = ViewModelProviders.of(this).get(TherapyViewModel.class);


        durationLayout = (LinearLayout) view.findViewById(R.id.duration_layout);
        remainderTime = (LinearLayout) view.findViewById(R.id.remainder_time_layout);


        pillName = (TextView) view.findViewById(R.id.pill_name);
        pillName.setText(mTherapy.getPillName());

        pillCompany = (TextView) view.findViewById(R.id.pill_company);
        pillCompany.setText(mTherapy.getPillCompany());

        pillAdvice = (TextView) view.findViewById(R.id.pill_advice);
        pillAdvice.setText(mTherapy.getIntakeAdvice());

        pillUnit = (TextView) view.findViewById(R.id.pill_unit);
        pillUnit.setText(mTherapy.getPillUnit());

        pillDuration = (TextView) view.findViewById(R.id.pill_duration);
        chosenDurationStatus = (TextView) view.findViewById(R.id.chosenDurationStatus);

        pref = SharedPreferenceManager.getInstance(getActivity());
        if (pref.getDuration().getUntil_date() != null || pref.getDuration().getFor_x_day() > 0 || pref.getDuration().getDuration_status() != null) {
            String duration_status = pref.getDuration().getDuration_status();
            if (duration_status.equals(Constants.NO_END_DATE)) {
                pillDuration.setText(getString(R.string.no_end_date));
            } else if (duration_status.equals(Constants.UNTIL_DATE)) {
                pillDuration.setText(getString(R.string.until_date));
                chosenDurationStatus.setText(pref.getDuration().getUntil_date());
            } else if (duration_status.equals(Constants.FOR_X_DAY)) {
                pillDuration.setText(getString(R.string.for_x_days));
                chosenDurationStatus.setText(String.valueOf(pref.getDuration().getFor_x_day()));
            }
        } else {
            pillDuration.setText(getString(R.string.no_end_date));
        }


        //navigation for the X button
        ImageButton imgBackArrow = (ImageButton) view.findViewById(R.id.ibBackArrow);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked X button");
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Discard changes?");
                alertDialog.setMessage("Do you really want to discard your changes?");
                alertDialog.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //remove previous fragment from the backstack (therefore navigating back)
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                });

                alertDialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog alertDialogBuilder = alertDialog.create();
                alertDialogBuilder.show();

            }
        });

        ImageButton imgSave = (ImageButton) view.findViewById(R.id.save_medication);
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                time_period = String.valueOf(hour) + ":" + String.valueOf(minute);
                if (time_period.isEmpty() || time_period.equals("00")) {
                    Toast.makeText(getActivity(), "Пожалуйста выберите время", Toast.LENGTH_SHORT).show();
                } else {
                    String pill_name=pillName.getText().toString().trim();
                    String pill_company=pillCompany.getText().toString().trim();
                    String pill_advice=pillAdvice.getText().toString().trim();
                    String pill_unit=pillUnit.getText().toString().trim();
                    int xDay;
                    String until_date;
                    String noDate;
                    if (pref.getDuration().getFor_x_day() != -1) {
                         xDay=pref.getDuration().getFor_x_day();

                    }else {
                        xDay=-1;
                    }
                    if(!pref.getDuration().getUntil_date().equals("undefined")) {
                        until_date=pref.getDuration().getUntil_date();
                    }else{
                        until_date="undefined";
                    }

                    if (!pref.getDuration().getNo_end_date().equals("undefined")) {
                         noDate=pref.getDuration().getNo_end_date();
                    }else{
                        noDate="undefined";
                    }

                    Therapy current_therapy=new Therapy(pill_name,pill_company,pill_advice,pill_unit,xDay,until_date,noDate,time_period);
                    current_therapy.setTherapyId(mTherapy.getTherapyId());
                    mTherapyViewModel.update(current_therapy);


                    Calendar calendar=Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY,hour);
                    calendar.set(Calendar.MINUTE,minute);
                    calendar.set(Calendar.SECOND,0);
                    setAlarm(calendar);
                    Toast.makeText(getActivity(), "Therapy successfully updated", Toast.LENGTH_SHORT).show();

                    ViewTherapyFragment fragment = new ViewTherapyFragment();
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(getString(R.string.add_medication_fragment));
                    transaction.commit();
                }


            }
        });

        durationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DurationFragment fragment = new DurationFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.duration_fragment));
                transaction.commit();
            }
        });

        remainderTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewGroup viewGroup = v.findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_remainder_time_picker_dialog, viewGroup, false);
                mNumberPickerHour = (NumberPicker) dialogView.findViewById(R.id.numberPickerHour);
                mNumberPickerMinute = (NumberPicker) dialogView.findViewById(R.id.numberPickerMinute);
                mNumberPickerHour.setMinValue(0);
                mNumberPickerHour.setMaxValue(24);
                mNumberPickerHour.setValue(8);
                mNumberPickerHour.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        return String.format("%02d", value);
                    }
                });

                mNumberPickerMinute.setMinValue(0);
                mNumberPickerMinute.setMaxValue(59);
                mNumberPickerMinute.setValue(0);
                mNumberPickerMinute.setFormatter(new NumberPicker.Formatter() {
                    @Override
                    public String format(int value) {
                        return String.format("%02d", value);
                    }
                });

                mNumberPickerHour.setOnValueChangedListener(onValueChangeListenerHour);
                mNumberPickerMinute.setOnValueChangedListener(onValueChangeListenerMinute);


                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(dialogView);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();

            }
        });


        return view;
    }

    private void setAlarm(Calendar calendar){
        AlarmManager alarmManager=(AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
        Intent intent=new Intent(getActivity(), AlertReceiver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(getActivity(),1,intent,0);

        if(calendar.before(Calendar.getInstance())){
            calendar.add(Calendar.DATE,1);
        }
        alarmManager.setExact(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),pendingIntent);
    }


    /**
     * Retrieves the selected therapy from the bundle (coming from TherapyActivity)
     *
     * @return
     */
    private Therapy getTherapyFromBundle() {
        Log.d(TAG, "getAdviceFromBundle: arguments: " + getArguments());

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            return bundle.getParcelable(getString(R.string.therapy));
        } else {
            return null;
        }
    }

    NumberPicker.OnValueChangeListener onValueChangeListenerHour = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            hour = picker.getValue();
        }
    };

    NumberPicker.OnValueChangeListener onValueChangeListenerMinute = new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            minute = picker.getValue();
        }
    };

    //these two override method below is responsible for toolbar action icon which is used to delete offer
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.delete,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_delete:
                Log.d(TAG, "onOptionsItemSelected: deleting contact.");
                mTherapyViewModel.deleteTherapyById(mTherapy.getTherapyId());
                Toast.makeText(getActivity(), "Therapy deleted successfully", Toast.LENGTH_SHORT).show();
                ViewTherapyFragment fragment = new ViewTherapyFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(getString(R.string.add_medication_fragment));
                transaction.commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
