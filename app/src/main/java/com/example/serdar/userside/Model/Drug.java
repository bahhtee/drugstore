package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Drug implements Parcelable {

    private Integer id;

    private String title;

    private Category category;

    private Producer producer;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.title);
        dest.writeParcelable(this.category, flags);
        dest.writeParcelable(this.producer, flags);
    }

    public Drug() {
    }

    protected Drug(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.category = in.readParcelable(Category.class.getClassLoader());
        this.producer = in.readParcelable(Producer.class.getClassLoader());
    }

    public static final Creator<Drug> CREATOR = new Creator<Drug>() {
        @Override
        public Drug createFromParcel(Parcel source) {
            return new Drug(source);
        }

        @Override
        public Drug[] newArray(int size) {
            return new Drug[size];
        }
    };

    @Override
    public String toString() {
        return title;
    }
}
