package com.example.serdar.userside.api;

import android.content.Context;

import com.example.serdar.userside.Utils.Constants;
import com.example.serdar.userside.Utils.SharedPrefManager;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

        public static Retrofit retrofit;

        public static void initClient(final Context context){

            final SharedPrefManager preference = SharedPrefManager.getInstance(context);
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            //this is the instance of OkHttp Library which is responsible for server time out when sending or getting requests
            //okhttp client is also responsible for adding headers(tokens) for Authorization
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(10,TimeUnit.SECONDS)
                    .writeTimeout(10,TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
              /*              if (preference.getUser().getToken()!= null) {
                                request = chain.request()
                                        .newBuilder()
                                        .addHeader("Authorization","Token "+preference.getUser().getToken())
                                        .addHeader("Content-Type", "application/json")
                                        .build();
                            }else {*/


                            return chain.proceed(request);

                        }
                    }).build();



            //after defining all requirements or laying foundation, retrofit is ready for communicating with server(base_url) holds pathway to server, gson library is for feting json from server
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        }


        public static Retrofit getClient() {
            return retrofit;
        }
}
