package com.example.serdar.userside;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.serdar.userside.Utils.App;
import com.example.serdar.userside.Utils.ConnectivityReceiver;

public class NoInternetActivity extends AppCompatActivity  implements SwipeRefreshLayout.OnRefreshListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private static final String TAG = "NoInternetActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);

        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout_internet_connection);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.text_color);

        checkConnection();



    }

    @Override
    public void onRefresh() {
        onLoadingSwipeRefresh();
    }


    private void onLoadingSwipeRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                checkConnection();
            }
        });
    }



    private void checkConnection() {
        boolean isConnected;
        isConnected = ConnectivityReceiver.isConnected();
        if(isConnected) {
            mSwipeRefreshLayout.setRefreshing(false);
            finish();
        }else {
            showSnack(isConnected);
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }

    private void showSnack(boolean isConnected) {
        String message;
        if (isConnected) {
            message = "Отлично, есть подключение к Интернету";
        } else {
            message = "Извините! нет подключения к Интернету";

        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        App.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
