package com.example.serdar.userside;

import android.content.Intent;

public class Events {

    public static class StatusNotification{
        private Intent intent;

        public StatusNotification(Intent intent) {
            this.intent = intent;
        }

        public Intent getIntent() {
            return intent;
        }
    }

}
