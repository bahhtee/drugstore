package com.example.serdar.userside.Storage;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.serdar.userside.Model.Therapy;

import java.util.List;

public class TherapyViewModel extends AndroidViewModel {
    private TherapyRepository mTherapyRepository;
    private LiveData<List<Therapy>> allTherapies;

    public TherapyViewModel(@NonNull Application application) {
        super(application);
        mTherapyRepository=new TherapyRepository(application);
        allTherapies=mTherapyRepository.getAllTherapies();
    }

    public void insert(Therapy therapy){
        mTherapyRepository.insert(therapy);
    }

    public void update(Therapy therapy){
        mTherapyRepository.update(therapy);
    }

    public void updatePillStatus(String morning, Integer id){
        mTherapyRepository.updatePillStatus(morning,id);
    }

    public void delete(Therapy therapy){
        mTherapyRepository.delete(therapy);
    }

    public void deleteAllTherapies(){
        mTherapyRepository.deleteAllTherapies();
    }

    public LiveData<List<Therapy>> getAllTherapies(){
        return allTherapies;
    }

    public void deleteTherapyById(Integer id){
        mTherapyRepository.deleteTherapyById(id);
    }
}
