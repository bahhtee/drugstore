package com.example.serdar.userside.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.RadioButton;

import com.example.serdar.userside.Model.Therapy;
import com.example.serdar.userside.R;

public class IntakeAdviceFragment extends Fragment {

    private static final String TAG = "IntakeAdviceFragment";
    private RadioButton rbNone, rbWithMeal,rbAfterMeal,rbBeforeMeal;
    private Therapy mTherapy;


    public interface OnSelectedAdviceListener{
        public void onSelectedAdvice(Therapy therapy);
    }

    OnSelectedAdviceListener mOnSelectedAdviceListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.intake_advice_fragment,container,false);
        mTherapy=getTherapyFromBundle();
        rbNone=(RadioButton)view.findViewById(R.id.none);
        rbNone.setChecked(true);
        rbWithMeal=(RadioButton)view.findViewById(R.id.with_meal);
        rbAfterMeal=(RadioButton)view.findViewById(R.id.after_meal);
        rbBeforeMeal=(RadioButton)view.findViewById(R.id.before_meal);


        //navigation for the backarrow
        ImageButton imgBackArrow = (ImageButton)view.findViewById(R.id.ibBackArrow);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Therapy therapy=new Therapy();
                therapy.setPillName(mTherapy.getPillName());
                therapy.setPillCompany(mTherapy.getPillCompany());
                therapy.setPillUnit(mTherapy.getPillUnit());
                Log.d(TAG, "onClick: clicked back arrow.");
                if(rbNone.isChecked()){
                    therapy.setIntakeAdvice("Ни один");
                    mOnSelectedAdviceListener.onSelectedAdvice(therapy);
                }else if(rbWithMeal.isChecked()){
                    therapy.setIntakeAdvice("Во время еды");
                    mOnSelectedAdviceListener.onSelectedAdvice(therapy);
                }else if(rbAfterMeal.isChecked()){
                    therapy.setIntakeAdvice("После еды");
                    mOnSelectedAdviceListener.onSelectedAdvice(therapy);
                }else if(rbBeforeMeal.isChecked()){
                    therapy.setIntakeAdvice("До еды");
                    mOnSelectedAdviceListener.onSelectedAdvice(therapy);
                }
            }
        });


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnSelectedAdviceListener=(OnSelectedAdviceListener)getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException "+e.getMessage() );
        }
    }

    /**
     * Retrieves the selected therapy from the bundle (coming from TherapyActivity)
     * @return
     */
    private Therapy getTherapyFromBundle(){
        Log.d(TAG, "getAdviceFromBundle: arguments: "+getArguments());

        Bundle bundle=this.getArguments();
        if(bundle!=null){
            return  bundle.getParcelable(getString(R.string.therapy));
        }else {
            return null;
        }
    }
}
