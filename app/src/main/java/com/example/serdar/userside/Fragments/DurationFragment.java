package com.example.serdar.userside.Fragments;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.serdar.userside.R;
import com.example.serdar.userside.Utils.Constants;
import com.example.serdar.userside.Utils.SharedPreferenceManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static android.support.v4.app.DialogFragment.STYLE_NORMAL;

public class DurationFragment extends Fragment {
    private static final String TAG = "DurationFragment";

    private LinearLayout startDate, untilDateLayout,forXDayLayout;
    private NumberPicker mNumberPicker;
    private TextView chosen_current_date, txtUntilDateDuration,txtUntilDateDurationStatus,txtForXDayDuration,txtForXDayDurationStatus;
    private Calendar c = Calendar.getInstance();
    private int mYear = c.get(Calendar.YEAR);
    private int mMonth = c.get(Calendar.MONTH);
    private int mDay = c.get(Calendar.DAY_OF_MONTH);
    private int numberPickerValue;
    private RadioButton rbNoEndDate, rbUntilDate, rbForXDays;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.duration_fragment, container, false);
        startDate = (LinearLayout) view.findViewById(R.id.chooseDate);
        untilDateLayout=(LinearLayout)view.findViewById(R.id.untilDateLayout);
        forXDayLayout=(LinearLayout)view.findViewById(R.id.forXDayLayout);

        chosen_current_date = (TextView) view.findViewById(R.id.chosen_date);
        rbNoEndDate = (RadioButton) view.findViewById(R.id.no_end_date);
        rbUntilDate = (RadioButton) view.findViewById(R.id.until_date);
        rbForXDays = (RadioButton) view.findViewById(R.id.for_x_days);

        txtUntilDateDuration=(TextView)view.findViewById(R.id.txtUntilDateDuration);
        txtUntilDateDurationStatus=(TextView)view.findViewById(R.id.txtUntilDateDurationStatus);

        txtForXDayDuration=(TextView)view.findViewById(R.id.txtForXDayDuration);
        txtForXDayDurationStatus=(TextView)view.findViewById(R.id.txtForXDayDurationStatus);

        final SharedPreferenceManager pref=SharedPreferenceManager.getInstance(getActivity());


        chosen_current_date.setText(chosenDate());


        if(pref.getDuration().getUntil_date()!=null|| pref.getDuration().getFor_x_day()>0||pref.getDuration().getDuration_status()!=null||pref.getDuration().getNo_end_date()!=null){
            String duration_status = pref.getDuration().getDuration_status();
            if (duration_status.equals(Constants.NO_END_DATE)) {
                untilDateLayout.setVisibility(View.GONE);
                forXDayLayout.setVisibility(View.GONE);
                rbNoEndDate.setChecked(true);
            } else if (duration_status.equals(Constants.UNTIL_DATE)) {
                forXDayLayout.setVisibility(View.GONE);
                untilDateLayout.setVisibility(View.VISIBLE);
                rbUntilDate.setChecked(true);
                if(pref.getDuration().getUntil_date()!=null){
                    txtUntilDateDurationStatus.setText(pref.getDuration().getUntil_date());
                }
            } else if (duration_status.equals(Constants.FOR_X_DAY)) {
                untilDateLayout.setVisibility(View.GONE);
                forXDayLayout.setVisibility(View.VISIBLE);
                rbForXDays.setChecked(true);
                if(pref.getDuration().getFor_x_day()>0){
                    txtForXDayDurationStatus.setText(String.valueOf(pref.getDuration().getFor_x_day()));
                }
            }
        }


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                        GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                        chosen_current_date.setText(sdf.format(c.getTime()));
                    }
                };

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), STYLE_NORMAL, mDateSetListener, mYear, mMonth, mDay);
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dialog.show();


            }
        });


        //navigation for the X button
        ImageButton imgBackArrow = (ImageButton) view.findViewById(R.id.ibBackArrow);
        imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rbNoEndDate.isChecked()){
                    SharedPreferenceManager.getInstance(getActivity()).saveDuration(Constants.NO_END_DATE,getString(R.string.no_end_date),"undefined",-1);
                }else if(rbUntilDate.isChecked()){
                    SharedPreferenceManager.getInstance(getActivity()).saveDuration(Constants.UNTIL_DATE,"undefined",txtUntilDateDurationStatus.getText().toString().trim(),-1);
                }else if(rbForXDays.isChecked()){
                    SharedPreferenceManager.getInstance(getActivity()).saveDuration(Constants.FOR_X_DAY,"undefined","undefined",numberPickerValue);
                }
                //remove previous fragment from the backstack (therefore navigating back)
                getActivity().getSupportFragmentManager().popBackStack();

            }
        });

        rbNoEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               untilDateLayout.setVisibility(View.GONE);
               forXDayLayout.setVisibility(View.GONE);
            }
        });

        rbUntilDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forXDayLayout.setVisibility(View.GONE);
                untilDateLayout.setVisibility(View.VISIBLE);
                if(pref.getDuration().getUntil_date()!=null){
                    txtUntilDateDurationStatus.setText(pref.getDuration().getUntil_date());
                }else {
                    txtUntilDateDurationStatus.setText(chosenDate());
                }

            }
        });

        rbForXDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                untilDateLayout.setVisibility(View.GONE);
                forXDayLayout.setVisibility(View.VISIBLE);
                if(pref.getDuration().getFor_x_day()>0){
                    txtForXDayDurationStatus.setText(String.valueOf(pref.getDuration().getFor_x_day()));
                }else {
                    txtForXDayDurationStatus.setText("10");
                }
            }
        });




        untilDateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            mYear = year;
                            mMonth = monthOfYear;
                            mDay = dayOfMonth;
                            GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                            txtUntilDateDurationStatus.setText(sdf.format(c.getTime()));
                        }
                    };

                    DatePickerDialog dialog = new DatePickerDialog(getActivity(), STYLE_NORMAL, mDateSetListener, mYear, mMonth, mDay);
                    dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    dialog.show();


            }
        });


        forXDayLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ViewGroup viewGroup= v.findViewById(android.R.id.content);
                View dialogView= LayoutInflater.from(getActivity()).inflate(R.layout.custom_number_picker_dialog,viewGroup,false);
                mNumberPicker=(NumberPicker)dialogView.findViewById(R.id.numberPicker);
                mNumberPicker.setMinValue(1);
                mNumberPicker.setMaxValue(365);
                mNumberPicker.setValue(10);

                mNumberPicker.setOnValueChangedListener(onValueChangeListener);


                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
                builder.setView(dialogView);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                builder.setNegativeButton("отменить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog alertDialog=builder.create();
                alertDialog.show();
            }
        });




        return view;
    }

    private String chosenDate(){
        GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        return sdf.format(c.getTime());
    }

    NumberPicker.OnValueChangeListener onValueChangeListener=new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            numberPickerValue=picker.getValue();
            txtForXDayDurationStatus.setText(String.valueOf(numberPickerValue));
        }
    };
}
