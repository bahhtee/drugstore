package com.example.serdar.userside.Utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.example.serdar.userside.Events;
import com.example.serdar.userside.GlobalBus;
import com.example.serdar.userside.MyTherapy;
import com.example.serdar.userside.PillStatusActivity;
import com.example.serdar.userside.R;

public class NotificationHelper extends ContextWrapper {

    public static final String channelID="channel1ID";
    public static final String channelName="channel 1";

    private NotificationManager mNotificationManager;

    public NotificationHelper(Context base){
        super(base);
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            createChannels();
        }

    }

    @TargetApi(Build.VERSION_CODES.O)
    public void createChannels(){
        NotificationChannel channel=new NotificationChannel(channelID,channelName, NotificationManager.IMPORTANCE_HIGH);
        getManager().createNotificationChannel(channel);
    }

    public NotificationManager getManager(){
        if (mNotificationManager==null){
            mNotificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return mNotificationManager;
    }

    public NotificationCompat.Builder getChannelNotification(Intent intent){
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] v = {500,5000};

        Intent intent1=new Intent(this, PillStatusActivity.class);
        intent.setAction("My");
        intent.putExtra("pill_name_",intent.getStringExtra("pill_name"));
        intent.putExtra("pill_unit_",intent.getStringExtra("pill_unit"));
        intent.putExtra("pill_time_",intent.getStringExtra("pill_time"));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


//        Toast.makeText(this, ""+intent.getStringExtra("pill_name")+" "+intent.getStringExtra("pill_unit")+" "+intent.getStringExtra("pill_time"), Toast.LENGTH_SHORT).show();


        Events.StatusNotification statusNotification=new Events.StatusNotification(intent);
        GlobalBus.getBus().postSticky(statusNotification);


        return new NotificationCompat.Builder(getApplicationContext(),channelID)
                .setContentTitle(intent.getStringExtra("pill_name"))
                .setContentText(intent.getStringExtra("pill_unit"))
                .setVibrate(v)
                .setSound(uri)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(PendingIntent.getActivity(this,0,intent1,PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.drawable.ic_medical_pill)
                .setDefaults(Notification.DEFAULT_ALL);
    }




}
