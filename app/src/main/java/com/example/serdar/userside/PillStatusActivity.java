package com.example.serdar.userside;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.serdar.userside.Model.Therapy;
import com.example.serdar.userside.Storage.TherapyViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class PillStatusActivity extends AppCompatActivity {


   private TextView pillName,pillUnit,timeTxt;
   private Intent intent;
   private String pill_name,pill_unit,time_text;

   private RelativeLayout skipLayout,confirmLayout;
   private Toolbar toolbar;
   private Therapy therapy;
   private TherapyViewModel therapyViewModel;

   private BroadcastReceiver broadcastReceiver;
   private IntentFilter intentFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pill_status);

        toolbar=findViewById(R.id.toolbar2);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        therapyViewModel= ViewModelProviders.of(this).get(TherapyViewModel.class);


        pillName=findViewById(R.id.drugNameTxt);
        pillUnit=findViewById(R.id.drugUnitTxt);
        timeTxt=findViewById(R.id.timeTxt);

        skipLayout=findViewById(R.id.skipLayout);
        confirmLayout=findViewById(R.id.confirmLayout);

        intent=getIntent();

        intentFilter=new IntentFilter("Broadcast");


        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                pill_name=intent.getStringExtra("pill_name");
                pill_unit=intent.getStringExtra("pill_unit");
                time_text=intent.getStringExtra("pill_time");

                pillName.setText(pill_name);
                pillUnit.setText(pill_unit);
                timeTxt.setText(time_text);


            }
        };

       registerReceiver(broadcastReceiver,intentFilter);








        skipLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(PillStatusActivity.this,MyTherapy.class));
                finish();


//                therapy.setRtMorning("skipped");
//                therapy.setTherapyId(therapy.getTherapyId());
//                Toast.makeText(PillStatusActivity.this, "Skipped"+therapy.toString(), Toast.LENGTH_SHORT).show();
//                Log.d("dilshod",therapy.toString());
//                therapyViewModel.update(therapy);

            }
        });


        confirmLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                therapy.setRtMorning("confirmed");
//                therapy.setTherapyId(therapy.getTherapyId());
//                therapyViewModel.update(therapy);

                startActivity(new Intent(PillStatusActivity.this,MyTherapy.class));
                finish();

            }
        });



    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void yourEvent(Events.StatusNotification statusNotification){




//        pill_name=statusNotification.getIntent().getStringExtra("pill_name");
//        pill_unit=statusNotification.getIntent().getStringExtra("pill_unit");
//        time_text=statusNotification.getIntent().getStringExtra("pill_time");

        therapy=statusNotification.getIntent().getParcelableExtra("myTerapy");


       // Toast.makeText(this, ""+pill_name+" "+pill_unit+" "+time_text, Toast.LENGTH_SHORT).show();






//        pillName.setText(pill_name);
//        pillUnit.setText(pill_unit);
//        timeTxt.setText(time_text);





    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
