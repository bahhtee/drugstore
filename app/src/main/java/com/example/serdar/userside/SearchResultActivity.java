package com.example.serdar.userside;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.serdar.userside.Model.Offer;
import com.example.serdar.userside.Utils.Constants;
import com.example.serdar.userside.api.Api;
import com.example.serdar.userside.api.RetrofitClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private OfferAdapter mOfferAdapter;
    private ArrayList<Offer> mOfferArrayList;
    private ArrayList<Offer> newOfferList=new ArrayList<Offer>();
    private ArrayList<Offer> mRecommendedList=new ArrayList<Offer>();
    private ProgressDialog mProgressDialog;
    private Button btnRate,btnRecommended,btnPrice;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Intent intent=getIntent();
        mOfferArrayList=intent.getParcelableArrayListExtra(Constants.OFFER_LIST);

        Collections.sort(mOfferArrayList, new Comparator<Offer >() {
                    @Override
                    public int compare(Offer p1, Offer p2) {
                        return p2.getStore().getRate() - p1.getStore().getRate();
                    }
                });

        for (Offer offer:mOfferArrayList) {
            if(offer.getStatus().equals("Недоступно")){
                newOfferList.add(offer);
            }

        }

        btnRate=(Button)findViewById(R.id.btnRate);
        btnRecommended=(Button)findViewById(R.id.btnRecommended);
        btnPrice=(Button)findViewById(R.id.btnPrice);

        mRecyclerView=(RecyclerView)findViewById(R.id.rvOffers);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mOfferAdapter=new OfferAdapter(this,newOfferList);
        mRecyclerView.setAdapter(mOfferAdapter);



        findViewById(R.id.btnRecommended).setOnClickListener(this);
        findViewById(R.id.btnRate).setOnClickListener(this);
        findViewById(R.id.btnPrice).setOnClickListener(this);
        findViewById(R.id.ibBackArrow).setOnClickListener(this);
    }

    private void getOfferList(){
        mRecommendedList.clear();
        Map<String, String> parameters = new HashMap<>();
        parameters.put("drug__category", String.valueOf(newOfferList.get(0).getDrug().getCategory().getId()));

        mProgressDialog = new ProgressDialog(SearchResultActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("загрузка...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Api api= RetrofitClient.getClient().create(Api.class);
        Call<ArrayList<Offer>> call=api.getOfferList(parameters);

        call.enqueue(new Callback<ArrayList<Offer>>() {
            @Override
            public void onResponse(Call<ArrayList<Offer>> call, Response<ArrayList<Offer>> response) {
                if(response.isSuccessful()){
                    if(mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }
                    ArrayList<Offer> offers=response.body();
                    if(offers.size()>0){
                        for (Offer offer:offers) {
                            if(!newOfferList.get(0).getDrug().getTitle().equals( offer.getDrug().getTitle())){
                                mRecommendedList.add(offer);

                            }
                        }
                        mRecyclerView=(RecyclerView)findViewById(R.id.rvOffers);
                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(SearchResultActivity.this));
                        mOfferAdapter=new OfferAdapter(SearchResultActivity.this,mRecommendedList);
                        mRecyclerView.setAdapter(mOfferAdapter);
                    }else {
                        Toast.makeText(SearchResultActivity.this, "No drug is found", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Offer>> call, Throwable t) {
                if(mProgressDialog.isShowing()){
                    mProgressDialog.dismiss();
                }
                Intent intent=new Intent(SearchResultActivity.this,NoInternetActivity.class);
                startActivity(intent);
            }
        });
    }

    private void sortByRate(){
        mProgressDialog = new ProgressDialog(SearchResultActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("загрузка...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Collections.sort(newOfferList, new Comparator<Offer >() {
            @Override
            public int compare(Offer p1, Offer p2) {
                return p2.getStore().getRate() - p1.getStore().getRate();
            }
        });

        mOfferAdapter=new OfferAdapter(this,newOfferList);
        mRecyclerView.setAdapter(mOfferAdapter);

        if(mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    private void sortByPrice(){
        mProgressDialog = new ProgressDialog(SearchResultActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("загрузка...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Collections.sort(newOfferList, new Comparator<Offer >() {
            @Override public int compare(Offer p1, Offer p2) {
                return (int)(Double.parseDouble(p1.getPrice())) -(int)(Double.parseDouble(p2.getPrice())); // Ascending
            }
        });
        mOfferAdapter=new OfferAdapter(this,newOfferList);
        mRecyclerView.setAdapter(mOfferAdapter);

        if(mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRecommended:
                btnRecommended.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.holo_green_light), PorterDuff.Mode.MULTIPLY);
                btnRate.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                btnPrice.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                getOfferList();
                break;
            case R.id.ibBackArrow:
                finish();
                break;
            case R.id.btnRate:
                btnRate.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.holo_green_light), PorterDuff.Mode.MULTIPLY);
                btnRecommended.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                btnPrice.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                sortByRate();
                break;
            case R.id.btnPrice:
                btnPrice.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.holo_green_light), PorterDuff.Mode.MULTIPLY);
                btnRecommended.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                btnRate.getBackground().setColorFilter(ContextCompat.getColor(this,android.R.color.darker_gray), PorterDuff.Mode.MULTIPLY);
                sortByPrice();
                break;
        }
    }
}
