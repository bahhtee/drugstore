package com.example.serdar.userside.Utils;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.example.serdar.userside.api.RetrofitClient;

public class App extends MultiDexApplication {
    private static App mInstance;



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
       RetrofitClient.initClient(getApplicationContext());
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener){
        ConnectivityReceiver.connectivityReceiver =listener;
    }

    public static synchronized App getInstance(){
        return mInstance;
    }
}
