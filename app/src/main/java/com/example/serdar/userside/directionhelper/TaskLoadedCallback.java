package com.example.serdar.userside.directionhelper;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
