package com.example.serdar.userside;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputEditText;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.example.serdar.userside.Model.Offer;
import com.example.serdar.userside.Model.Region;
import com.example.serdar.userside.Utils.Constants;
import com.example.serdar.userside.api.Api;
import com.example.serdar.userside.api.RetrofitClient;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    private ProgressDialog mProgressDialog;
    private NumberPicker mNumberPicker;
    private ArrayList<Region> regionArrayList;
    List<String> regionList=new ArrayList<>();
    private int current_region_list_position;
    private int current_region_id;
    private TextInputEditText mSearchEditText;
    private MaterialButton btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mNumberPicker=findViewById(R.id.regionPicker);
        mSearchEditText=(TextInputEditText)findViewById(R.id.edtSearch);
        btnSearch=(MaterialButton)findViewById(R.id.btnSearch);

        getRegionList();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query=mSearchEditText.getText().toString().trim();
                if(query.isEmpty()){
                    Toast.makeText(MainActivity.this,  "Введите название лекарства", Toast.LENGTH_SHORT).show();
                }else {
                    getOfferList(query);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_therapy) {
           Intent intent=new Intent(MainActivity.this,MyTherapy.class);
           startActivity(intent);
        } else if (id == R.id.closest_drug_store) {

        } else if (id == R.id.about_us) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void getOfferList(String query){
        Map<String, String> parameters = new HashMap<>();
        parameters.put("search",query );
        parameters.put("store__region", String.valueOf(current_region_id));

        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("загрузка...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Api api=RetrofitClient.getClient().create(Api.class);
        Call<ArrayList<Offer>> call=api.getOfferList(parameters);

        call.enqueue(new Callback<ArrayList<Offer>>() {
            @Override
            public void onResponse(Call<ArrayList<Offer>> call, Response<ArrayList<Offer>> response) {
                if(response.isSuccessful()){
                    if(mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }
                    ArrayList<Offer> offers=response.body();
                    if(offers.size()>0){
                        Intent intent=new Intent(MainActivity.this,SearchResultActivity.class);
                        intent.putParcelableArrayListExtra(Constants.OFFER_LIST,offers);
                        startActivity(intent);
                    }else {
                        Toast.makeText(MainActivity.this, "Лекарство не найдено", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Offer>> call, Throwable t) {
                if(mProgressDialog.isShowing()){
                    mProgressDialog.dismiss();
                }
                Intent intent=new Intent(MainActivity.this,NoInternetActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getRegionList(){

        mProgressDialog = new ProgressDialog(MainActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("загрузка...");
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        Api api= RetrofitClient.getClient().create(Api.class);

        Call<ArrayList<Region>> call=api.getRegionList();

        call.enqueue(new Callback<ArrayList<Region>>() {
            @Override
            public void onResponse(Call<ArrayList<Region>> call, Response<ArrayList<Region>> response) {
                if(response.isSuccessful()){
                   regionArrayList=response.body();

                    for (Region region:regionArrayList) {
                        if(region.getTitle()!=null){
                                regionList.add(region.getTitle());
                            }
                    }
                    String [] data=regionList.toArray(new String[regionList.size()]);
                    mNumberPicker.setMinValue(0);
                    mNumberPicker.setMaxValue(data.length-1);
                    mNumberPicker.setDisplayedValues(data);
                    mNumberPicker.setOnValueChangedListener(OnRegionChangedListener);
                    if(mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }

                }else {
                    if(mProgressDialog.isShowing()){
                        mProgressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Region>> call, Throwable t) {
                if(mProgressDialog.isShowing()){
                    mProgressDialog.dismiss();
                }
                Intent intent=new Intent(MainActivity.this,NoInternetActivity.class);
                startActivity(intent);
            }
        });

    }

    NumberPicker.OnValueChangeListener OnRegionChangedListener=new NumberPicker.OnValueChangeListener() {
        @Override
        public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
            current_region_list_position =picker.getValue();
            String region_title=regionList.get(current_region_list_position);
            for (Region region:regionArrayList) {
                if(region.getTitle().equals(region_title)){
                    current_region_id=region.getId();
                }

            }
        }
    };

}
