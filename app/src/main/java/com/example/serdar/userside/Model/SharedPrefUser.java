package com.example.serdar.userside.Model;

public class SharedPrefUser {

    private int userId;
    private String token;

    public SharedPrefUser(int userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
