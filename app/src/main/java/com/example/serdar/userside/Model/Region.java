package com.example.serdar.userside.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Region implements Parcelable {

    private int id;

    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
    }

    public Region() {
    }

    protected Region(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
    }

    public static final Creator<Region> CREATOR = new Creator<Region>() {
        @Override
        public Region createFromParcel(Parcel source) {
            return new Region(source);
        }

        @Override
        public Region[] newArray(int size) {
            return new Region[size];
        }
    };

    @Override
    public String toString() {
        return "Region{" +
                "title='" + title + '\'' +
                '}';
    }
}
