package com.example.serdar.userside.Storage;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.serdar.userside.Model.Therapy;

import java.util.List;

public class TherapyRepository {
    private TherapyDoa mTherapyDoa;
    private LiveData<List<Therapy>> allTherapies;

    public TherapyRepository(Application application){
        MyDrugStoreDatabase database=MyDrugStoreDatabase.getDatabase(application);
        mTherapyDoa=database.mTherapyDoa();
        allTherapies=mTherapyDoa.getAllTherapies();
    }

    public void insert(Therapy therapy){
        new InsertTherapyTask(mTherapyDoa).execute(therapy);
    }

    public void update(Therapy therapy){
        new UpdateTherapyAsyncTask(mTherapyDoa).execute(therapy);
    }

    public void updatePillStatus(String morning,Integer id){
       // mTherapyDoa.updatePillStatus(morning,id);
    }


    public void delete(Therapy therapy){
        new DeleteTherapyAsyncTask(mTherapyDoa).execute(therapy);
    }

    public void deleteTherapyById(Integer id){
        new DeleteTherapyByIdAsyncTask(mTherapyDoa).execute(id);
    }

    public void deleteAllTherapies(){
        new DeleteAllTherapiesAsyncTask(mTherapyDoa).execute();
    }

    public LiveData<List<Therapy>> getAllTherapies(){
        return allTherapies;
    }

    private static class InsertTherapyTask extends AsyncTask<Therapy,Void,Void>{
        private TherapyDoa mTherapyDoa;

        private InsertTherapyTask(TherapyDoa therapyDoa){
            this.mTherapyDoa=therapyDoa;
        }


        @Override
        protected Void doInBackground(Therapy... therapies) {
            mTherapyDoa.insert(therapies[0]);
            return null;
        }
    }


    private static class UpdateTherapyAsyncTask extends AsyncTask<Therapy,Void,Void>{
        private TherapyDoa mTherapyDoa;

        private UpdateTherapyAsyncTask(TherapyDoa therapyDoa){
            this.mTherapyDoa=therapyDoa;
        }

        @Override
        protected Void doInBackground(Therapy... therapies) {
            mTherapyDoa.update(therapies[0]);
            return null;
        }
    }


    private static class DeleteTherapyAsyncTask extends AsyncTask<Therapy,Void,Void>{
        private TherapyDoa mTherapyDoa;

        private DeleteTherapyAsyncTask(TherapyDoa therapyDoa){
            this.mTherapyDoa=therapyDoa;
        }

        @Override
        protected Void doInBackground(Therapy... therapies) {
            mTherapyDoa.delete(therapies[0]);
            return null;
        }
    }

    private static class DeleteAllTherapiesAsyncTask extends AsyncTask<Void,Void,Void>{
        private TherapyDoa mTherapyDoa;

        private DeleteAllTherapiesAsyncTask(TherapyDoa therapyDoa){
            this.mTherapyDoa=therapyDoa;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mTherapyDoa.deleteAllTherapies();
            return null;
        }
    }


    private class DeleteTherapyByIdAsyncTask extends AsyncTask<Integer,Void,Void>{
        private TherapyDoa mTherapyDoa;

        DeleteTherapyByIdAsyncTask(TherapyDoa therapyDoa){
            mTherapyDoa=therapyDoa;

        }

        @Override
        protected Void doInBackground(Integer... integers) {
            mTherapyDoa.deleteTherapyById(integers[0]);
            return null;
        }
    }



}
