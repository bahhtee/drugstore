package com.example.serdar.userside;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.serdar.userside.Model.Therapy;

import java.util.List;

public class TherapyAdapter extends RecyclerView.Adapter<TherapyAdapter.ViewHolder>{

    private Context mContext;
    private List<Therapy> mTherapies;
    private OnItemClickListener mListener;



    public TherapyAdapter(Context context, List<Therapy> therapies){
        this.mContext=context;
        this.mTherapies=therapies;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener)   {
        mListener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(mContext).inflate(R.layout.therapy_list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TherapyAdapter.ViewHolder viewHolder, int position) {
        Therapy current_therapy=mTherapies.get(position);

        String pillName=current_therapy.getPillName();
        viewHolder.pillName.setText(pillName);

        String intake_advice=current_therapy.getIntakeAdvice();
        viewHolder.intakeAdviceStatus.setText(intake_advice);
        viewHolder.intakeAdvice.setText(R.string.intake_advice_text);


        if(current_therapy.getForXDay()!=-1){
            int pillDurationDays=current_therapy.getForXDay();
            viewHolder.pillState.setText(String.valueOf(pillDurationDays));
            viewHolder.pillStatus.setText(R.string.for_days);
        }
        if(!current_therapy.getNoEndDate().equals("undefined")){
            String noEndDate=current_therapy.getNoEndDate();
            viewHolder.pillState.setText(noEndDate);
            viewHolder.pillStatus.setText(R.string.duration_text);
        }

        if(!current_therapy.getUntilDate().equals("undefined")){
            String untilDate=current_therapy.getUntilDate();
            viewHolder.pillState.setText(untilDate);
            viewHolder.pillStatus.setText(R.string.until_date_text);
        }



        if(current_therapy.getRtMorning()!=null){

            if(current_therapy.getRtMorning().equals("skipped")){

                viewHolder.itemView.setBackgroundColor(Color.RED);
            }

        } else {


        }






    }

    @Override
    public int getItemCount() {
        return mTherapies.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView pillName, pillState, pillStatus, intakeAdvice, intakeAdviceStatus;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pillName=(TextView)itemView.findViewById(R.id.pill_name);
            pillState =(TextView)itemView.findViewById(R.id.pill_state);
            pillStatus =(TextView)itemView.findViewById(R.id.status);

            intakeAdvice =(TextView)itemView.findViewById(R.id.intakeAdvice);
            intakeAdviceStatus =(TextView)itemView.findViewById(R.id.advice_value);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);
                        }
                    }
                }
            });

        }
    }

}
