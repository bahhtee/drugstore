package com.example.serdar.userside.Model.SharedPreferenceDurationModel;

public class SharedPreferenceDuration {
    private String duration_status;
    private String no_end_date;
    private String until_date;
    private int for_x_day;

    public SharedPreferenceDuration(String duration_status, String no_end_date, String until_date, int for_x_day) {
        this.duration_status = duration_status;
        this.no_end_date = no_end_date;
        this.until_date = until_date;
        this.for_x_day = for_x_day;
    }

    public String getDuration_status() {
        return duration_status;
    }

    public void setDuration_status(String duration_status) {
        this.duration_status = duration_status;
    }

    public String getNo_end_date() {
        return no_end_date;
    }

    public void setNo_end_date(String no_end_date) {
        this.no_end_date = no_end_date;
    }

    public String getUntil_date() {
        return until_date;
    }

    public void setUntil_date(String until_date) {
        this.until_date = until_date;
    }

    public int getFor_x_day() {
        return for_x_day;
    }

    public void setFor_x_day(int for_x_day) {
        this.for_x_day = for_x_day;
    }
}
