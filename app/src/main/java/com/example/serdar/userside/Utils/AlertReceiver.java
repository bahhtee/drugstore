package com.example.serdar.userside.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class AlertReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

    //  Bundle bundle=intent.getBundleExtra("myBundle");

       // Toast.makeText(context, ""+bundle.getString("pill_name"), Toast.LENGTH_SHORT).show();

        NotificationHelper notificationHelper=new NotificationHelper(context);
        NotificationCompat.Builder nb=notificationHelper.getChannelNotification(intent);
        notificationHelper.getManager().notify(1,nb.build());
    }
}
