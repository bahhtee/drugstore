package com.example.serdar.userside;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.serdar.userside.Model.Offer;
import com.example.serdar.userside.Utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

public class OfferAdapter extends  RecyclerView.Adapter<OfferAdapter.ViewHolder> {
    private static final String TAG = "OfferAdapter";
    private static final int ERROR_DIALOG_REQUEST=9001;

    private Context mContext;
    private List<Offer> mOfferList;



    public OfferAdapter(Context context, List<Offer> offers){
        this.mContext=context;
        this.mOfferList=offers;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.offer_list_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Offer current_offer=mOfferList.get(position);

        if(current_offer.getDrug()!=null){
            viewHolder.pillName.setText(current_offer.getDrug().getTitle());
        }

        if(current_offer.getStore()!=null){
            viewHolder.storeName.setText(current_offer.getStore().getTitle());
            if(current_offer.getStore().getAddress()!=null){
                viewHolder.storeAddress.setText(current_offer.getStore().getAddress().get(0).getLine());
            }
        }

        if(current_offer.getPrice()!=null){
            viewHolder.pillPrice.setText(current_offer.getPrice());
        }



    }

    @Override
    public int getItemCount() {
        return mOfferList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView pillName, storeName, storeAddress, pillPrice;
        private ImageButton btnCall, btnMap, btnShare;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            pillName=(TextView)itemView.findViewById(R.id.pill_name);
            storeName=(TextView)itemView.findViewById(R.id.txtDrugStore);
            storeAddress=(TextView)itemView.findViewById(R.id.txtAddressValue);
            pillPrice=(TextView)itemView.findViewById(R.id.txtPriceValue);
            btnCall=(ImageButton)itemView.findViewById(R.id.call);
            btnMap=(ImageButton)itemView.findViewById(R.id.map);

            btnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Offer offer=mOfferList.get(getAdapterPosition());
                    String store_phone=offer.getStore().getAddress().get(0).getPhone();
                    Intent callIntent=new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",store_phone,null));
                    mContext.startActivity(callIntent);

                }
            });
            if(isServiceOk()){
                btnMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Offer offer=mOfferList.get(getAdapterPosition());
                        String latitude=offer.getStore().getAddress().get(0).getLatitude();
                        String longitude=offer.getStore().getAddress().get(0).getLongitude();
                        String address_title=offer.getStore().getAddress().get(0).getLine();
                        Intent intent=new Intent(mContext,MapsActivity.class);
                        intent.putExtra(Constants.LATITUDE,latitude);
                        intent.putExtra(Constants.LONGITUDE,longitude);
                        intent.putExtra(Constants.ADDRESS_TITLE,address_title);
                        mContext.startActivity(intent);
                    }
                });
            }


        }
    }


    //checks google map is ready to use or not
    public boolean isServiceOk(){
        Log.d(TAG, "isServiceOk: checking google services version");

        int available= GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext);
        if(available== ConnectionResult.SUCCESS){
            //everything is fine and the user can make requests
            Log.d(TAG, "isServiceOk: Google Play Services is working");
            return true;
        }else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            //an error occured but we can resolve it
            Log.d(TAG, "isServiceOk: an error occured but we can fix it");
        }else{
            Toast.makeText(mContext, "Вы не можете делать запросы карты", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}
